# Copyright 2009 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="GooCanvas is a canvas widget for GTK+ using the cairo 2D library for drawing"
HOMEPAGE="https://wiki.gnome.org/Projects/GooCanvas"

LICENCES="LGPL-2"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gobject-introspection
    gtk-doc
    python
"

DEPENDENCIES="
    build:
        dev-util/pkg-config
        sys-devel/gettext[>=0.19.4]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.8] )
    build+run:
        dev-libs/atk
        dev-libs/glib:2[>=2.28.0]
        x11-libs/cairo[>=1.10.0]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.0.0][gobject-introspection?]
        x11-libs/pango
        python? ( gnome-bindings/pygobject:3[>=2.90.4] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Otherwise .gir file is installed to /usr/${host}/share/...
    --prefix=/usr
    --includedir=/usr/$(exhost --target)/include

    '--disable-rebuilds'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'python'
)

