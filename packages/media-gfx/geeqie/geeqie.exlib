# Copyright 2009, 2017 Marvin Schmidt <marv@exherbo.org>
# Copyright 2015 Alexander Scheuermann <alex+exherbo@tnix.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user=BestImageViewer suffix=tar.xz release=v${PV} ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require lua [ whitelist="5.3" with_opt=true multibuild=false ]

export_exlib_phases src_prepare

SUMMARY="Lightweight GTK+ based image viewer"

HOMEPAGE="https://www.${PN}.org/ ${HOMEPAGE}"
LICENCES="GPL-2"
SLOT="0"

MYOPTIONS="
    archive [[ description = [ Support for compressed images ] ]]
    clutter [[ description = [ Enable GPU accelerated display using clutter (experimental) ] ]]
    exiv2 [[ description = [ Enhanced EXIF support ] ]]
    geolocation [[
        description = [ Enable map display support for geo-tagged images ]
        requires = clutter
    ]]
    jpeg2000
    jpegxl [[ description = [ Support JPEG XL image format ] ]]
    lcms
    lirc
    pdf
    raw [[ description = [ Support RAW images ] ]]
    tiff
    webp
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas:
        ar be bg ca cs da de el eo es et eu fi fr hu id it ja ko nb nl pl pt_BR ro ru sk sl sr@latin
        sr sv th tlh tr uk vi zh_CN zh_TW
    )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.52.0]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        archive? ( app-arch/libarchive[>=3.4.0] )
        clutter? (
            x11-libs/clutter:1
            x11-libs/clutter-gtk:1.0
        )
        exiv2? ( graphics/exiv2:=[>=0.11] )
        geolocation? ( x11-libs/libchamplain:0.12 )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.3.0] )
        jpegxl? ( media-libs/libjxl:=[>=0.3.7] )
        lcms? ( media-libs/lcms2 )
        lirc? ( app-misc/lirc )
        pdf? ( app-text/poppler[>=0.62] )
        raw? ( media-libs/libraw[>=0.20] )
        tiff? ( media-libs/tiff )
        webp? ( media-libs/libwebp:=[>=0.6.1] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-largefile
    --enable-nls
    --enable-gtk3
    --enable-threads
    --enable-jpeg
    --with-readmedir=/usr/share/doc/${PNV}

    --disable-djvu
    --disable-heif
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    archive
    'clutter gpu-accel'
    exiv2
    'geolocation map'
    jpegxl
    lcms
    lirc
    raw
    tiff
)

geeqie_src_prepare() {
    # required to support cross layout
    edo intltoolize --force --automake

    # Strip Changelog target as it attempts to read git log
    edo sed -E "/^readme_DATA/ s|ChangeLog(.html)?||g" -i Makefile.am

    # Our lua installs a pkgconfig named "lua-${SLOT}"
    edo sed -e '/PKG_CHECK_MODULES/s:lua\([0-9]\.[0-9]\):lua-\1:' \
            -i configure.ac

    autotools_src_prepare
}

