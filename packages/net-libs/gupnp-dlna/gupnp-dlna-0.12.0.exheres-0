# Copyright 2011 Dan Callaghan <djc@djc.id.au>
# Copyright 2022 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true vala_dep=true ]
require meson

SUMMARY="GUPnP DLNA is a small utility library that aims to ease the DLNA-related tasks
such as media profile guessing, transcoding to a given profile, etc."
HOMEPAGE="http://gupnp.org/"

LICENCES="LGPL-2.1"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.36.0] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.34.0]
        dev-libs/libxml2:2.0[>=2.5.0]
        media-libs/gstreamer:1.0
        media-plugins/gst-plugins-base:1.0
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddefault_backend=gstreamer
    -Dgstreamer_backend=enabled
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'vapi'
)

pkg_setup() {
    vala_pkg_setup
    meson_pkg_setup
}

